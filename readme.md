## Jay's Board Game Table

This repository houses the code used to run my board game table with end of turn buttons. More information about the project can be found here: https://www.instructables.com/Board-Game-Table-With-End-Turn-Buttons/ 
     
The actual config files for various games can be found in this repository: https://gitlab.com/jason.watson/board-game-table-games
     
View more of my projects here: https://www.claireandjay.ca 
import React from 'react'
import Seat from '../components/Seat'
import Screen from '../components/Screen'
import PhaseIndicator from '../components/PhaseIndicator'
import Video from '../components/Video'
import {socket} from "../service/socket";


import '../assets/stylesheets/App.css';

import useSound from 'use-sound';

function MobileViewAll() {
    
    const audio_folder =  `${process.env.PUBLIC_URL}/tmp/system-assets/audio`;
    const [tableData, setTableData] = React.useState(null)  
    // var socket = null;

    React.useEffect(()=>{
      // socket = io(socket_address)
      socket.on('connect', ()=>console.log(socket.id))
      socket.on('connect_error', ()=>{
        console.log("connection error")
        setTableData({})
        setTimeout(()=>socket.connect(),5000)
      })

      socket.on('table', function(data){        
        setTableData(data)
        console.log(`${new Date()} - table data received`)
        console.log(data)
      });

      socket.on('sound', function(data){      
        var sound_file = `${audio_folder}/${data}.wav`             
        console.log(`${new Date()} - sound data received`)
        console.log(`playing ${sound_file}`)
        var audio = new Audio(sound_file);
        audio.play();
      });

      socket.on('disconnect',()=>setTableData({}))
    },[])


    return (
      <div className="MobileViewAll">        
        <div className="container">
          <div className="col col-25" >
              <div className=" col-content">          
                <Screen tableData={tableData} />  
                <PhaseIndicator tableData={tableData} /> 
                <Seat socket={socket} tableData={tableData} index={0} />
                <Seat socket={socket} tableData={tableData} index={1} />
                <Seat socket={socket} tableData={tableData} index={2} />
                <Seat socket={socket} tableData={tableData} index={3} />  
                <Seat socket={socket} tableData={tableData} index={4} />
                <Seat socket={socket} tableData={tableData} index={5} />         
            </div>    
          </div>    
        </div>    
      </div>      
    )
} 

export default MobileViewAll;

import {io} from 'socket.io-client'
var config = require(`../tmp/settings.json`);
const socket_address = `http://${config.server_address}:${config.server_port}`;
export const socket = io(socket_address);

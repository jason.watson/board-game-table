// App.js
import { Routes, Route } from 'react-router-dom';
import React, { Component } from 'react';

import TableView from './pages/TableView';
import MobileViewAll from './pages/MobileViewAll';
import MobileView from './pages/MobileView';
import MobileViewSelect from './pages/MobileViewSelect';
import Navigation from './components/Navigation';

class App extends Component {

    render() {

        var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        windowWidth = w.innerWidth || e.clientWidth || g.clientWidth; //window width

        var is_mobile =  windowWidth <= 800; //returns true for widths larger than 568 pixels

        if(is_mobile){
            return(
                <>
                    <Navigation />
                    <Routes>
                        <Route path="/" element={<MobileViewSelect />} />
                        <Route path="/mobile/all" element={<MobileViewAll />} />
                        <Route path="/mobile/:seat_index" element={<MobileView />} />
                    </Routes>
                </>
            );
        } else {
            return(
                <>
                <Routes>
                    <Route path="/" element={<TableView />} />
                </Routes>
                </>
            );
        }

    };

};

export default App;
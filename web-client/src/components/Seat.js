import React, { Component } from 'react';
import PlayerName from './PlayerName.js'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { 
  faXmarkCircle,  
  faCircle, faCircleHalfStroke,
  faCircleChevronRight,
  faSkullCrossbones,
 } from '@fortawesome/free-solid-svg-icons'
 import {socket} from "../service/socket";
 var config = require(`../tmp/settings.json`);






 
class Seat extends Component {
  constructor(props){
    super(props);
    this.state = {count: 0, tableData: props.tableData};
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tickTimer(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
   
  }

  tickTimer() {
    if(!this.props.tableData){
      return null;
    } else {
      var seat = this.getSeat(this.props.index)
      if(seat == null || seat.status == null || seat.current_color.name == null ){
        return null
      } else if(seat.status.includes("active")){
        var new_count = this.state.count + 1
        this.setState({
          count: new_count
        });
      }else if(seat.status == "empty"){
        this.setState({
          count: 0
        });
      } else {       
        return null;
      }
    }
  }


  getTimer(){
    var date = new Date(null);
    date.setSeconds(this.state.count); 
    return date.toISOString().substr(11, 8);
  }
  getIcon(seat){
    if(seat == null || seat.status == null || seat.current_color.name == null ){
      return faXmarkCircle;

    }else if(seat.status == "dead"){
      return faSkullCrossbones;

    }else if(seat.status == "passed"){
      return faCircleHalfStroke;

    }else if(seat.status.includes("active")){
      return faCircleChevronRight;
      
    }else{
      return faCircle;
    }
  }
  getSeat(index){
    if(this.props.tableData == null || this.props.tableData.seats == null){
      return this.getEmptySeat();
    }else{
      var seat = this.props.tableData.seats[index]
      if(seat == null || seat.status == null || seat.current_color.name == null ){
        return this.getEmptySeat();
      } else {
        return seat
      }
    }
  }
  getEmptySeat(){
    var assumed_seat = config.seats[this.props.index];
    return {b1: assumed_seat.key1, b2: assumed_seat.key2, b3: assumed_seat.key3, b4: assumed_seat.key4, name: "", status: "empty", status_display: "", current_color: {name: "white"} }
  }

  pressTableKey(key) {
    console.log("table keypress sent " + key)
    socket.emit("pressTableKey", {keypress: key});
  }

  render() {

    if(this.state.tableData == null){
      var table = this.props.tableData
    } else {
      var table = this.state.tableData
    }
  
    var seat = this.getSeat(this.props.index)
    var icon = this.getIcon(seat)
    var timer = this.getTimer();
    var hide_style = {};

    var current_mode = table?.current_game?.current_mode

    if(current_mode == "message"){
      hide_style = {"display" : "none"}
        
    }
    
    return ([
        <div style={hide_style}  className={`seat status-${seat.status} color-${seat.current_color.name}`} id={"seat-"+this.props.index}>     
          <div className="player-seat-number">P{(parseInt(this.props.index)+1)}</div>
          <div className="player-container">              
          <div className="player-info">            
            <FontAwesomeIcon className="indicator" icon={icon} />
            <PlayerName className="name" seatIndex={this.props.index}  playerName={seat.name} />
            <span className="status">{seat.status_display}</span>          
            <span className="timer">{timer}</span>
          </div>
          <div className="player-buttons">
            <button className="player-button b1" onClick={() => this.pressTableKey(seat.b1)} title="Usually used to select your color or end your turn" >B1</button>
            <button className="player-button b2" onClick={() => this.pressTableKey(seat.b2)} title="Used by some games to pass your turn">B2</button>
            <button className="player-button b3" onClick={() => this.pressTableKey(seat.b3)} title="Used to kill your player and opt out of the current game" >B3</button>
            <button className="player-button b4" onClick={() => this.pressTableKey(seat.b4)} title="Used by P1 to end the game">B4</button>
          </div>
          </div>
        </div>
    ]);
    
  }


}
export default Seat;
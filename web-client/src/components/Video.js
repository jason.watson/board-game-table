import React, { Component } from 'react';
import YouTube from 'react-youtube';


class Video extends Component {

  constructor(props){
    super(props);

    this.state = {
      page_ready: false,
      current_video: null
    }
    this.startPage = this.startPage.bind(this)
  }

  startPage(){
    this.setState({page_ready : true})
  }


  render() {

    const youtube_opts = {
      height: '900',
      width: '1600',
      playerVars: {
        controls: 0,
        autoplay: 1,
        mute: 0,
        loop: 1,
        modestbranding: 1,
        showinfo: 0,
        rel: 0
        },
    };
    var youtube_id = this.props.tableData?.current_game?.config?.background_youtube_id
    var video_url= this.props.tableData?.current_game?.config?.background_video_url
    var current_table_mode = this.props.tableData?.mode

    if(this.props.tableData == null || this.props.tableData.seats == null){
      return([
        <div className="overlay">
          <div>Waiting for Table...</div>
        </div>
      ])

    }else if(this.state.page_ready == false){
      return([
        <div className="overlay" onClick={this.startPage}>
          <div>Click To Start</div>
        </div>
      ])

    }else if((youtube_id == null && video_url == null) || current_table_mode != "playing-game"){
      return null;
    
    
    } else if (youtube_id != null){
      
      return ([
        <div className="videoContainer">
          <div className="videoCoverUp"></div>
          <YouTube className="video" videoId={youtube_id} opts={youtube_opts} onClick={this.play} />
        </div>
      ])
    
    } else if (video_url != null){

      return ([
        <div className="videoContainer">
          <div className="videoCoverUp"></div>
          <video className="videoPlayer" src={video_url} autoplay="autoplay" loop="loop"  />
        </div>
      ])

    }

         
  }

  play(event) {
    event.target.playVideo();
  }




}
export default Video;
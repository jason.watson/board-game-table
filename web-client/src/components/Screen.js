import React, { Component } from 'react';
import {socket} from "../service/socket";
var config = require(`../tmp/settings.json`);

class Screen extends Component {

    pressTableKey(key) {
        console.log("table keypress sent " + key)
        socket.emit("pressTableKey", {keypress: key});
    }

    render() {
        var table = this.props.tableData

        var system_folder =  `${process.env.PUBLIC_URL}/tmp/system-assets`;
        var games_folder =  `${process.env.PUBLIC_URL}/tmp/games`;
        var image_path = "";
        var message = false;

        if(table == null || table.seats.null){
            var p1b2_key = config.seats[0].key1
        } else {
            var p1b2_key = table.seats[0].b2
        }
        

        if(table != null){
            var table_mode = table.mode
            var game = table.current_game
            var message_text = ""
            var next_text = ""
            var setting_index = table.setting_index
            var screen_styles = {}
            console.log(setting_index)


            if (
                table && 
                table.current_game && 
                table.current_game.current_phase && 
                table.current_game.current_phase.message_text !== null
            ) {
                message_text = table.current_game.current_phase.message_text;
                screen_styles = {opacity: 0.5}
            }
            if (
                table && 
                table.current_game && 
                table.current_game.current_phase && 
                table.current_game.current_phase.next_text !== null
            ) {
                next_text = table.current_game.current_phase.next_text;
            }

            switch(table_mode) {
                case "choosing-game-mode":                      
                    image_path = `${games_folder}/${game.id}/${game.config.image_folder}/${game.current_game_mode.image}`                
                    break;

                case "playing-game":      
                    var mode = table.current_game.current_phase.mode      
                    if(mode == "message"){                    
                        message=true
                    } else {
                        image_path = `${games_folder}/${game.id}/${game.config.image_folder}/${game.current_phase.image}`                
                     }         
                    break;

                case "changing-settings": 
                    if(setting_index == 0){
                        image_path = `${system_folder}/images/shutdown.png`  
                    }      
                    break;

                case "shutting-down": 
                    image_path = `${system_folder}/images/shutting-down.png`       
                    break;

                case "choosing-game":  
                    image_path = `${games_folder}/${game.id}/${game.config.select_image}`                
                    break;                

                case "adding-players":  
                    image_path = `${system_folder}/images/add-players.png`                
                    break;

                case "choosing-first-player":  
                    image_path = `${system_folder}/images/select-first-player.png`                
                    break;


                case "idle":  
                    image_path = `${system_folder}/images/idle.png`                
                    break;               

                default:
                    image_path = ""
            }
        }

        if(message){
            return ([            
                <div className='Message'>
                    <div className='message-container'>
                    <div className="message-text">
                        <p> {message_text} </p>
                    </div>
                    <div className="message-buttons">
                        <button className="message-button p1b2" onClick={() => this.pressTableKey(p1b2_key)} >P1B2</button>
                    </div>
                    </div>
                </div>,
            ]);
        }else{
            return ([            
                <div className='Screen'>
                    <img style={screen_styles} 
                        src={image_path}
                        onError={({ currentTarget }) => {
                            currentTarget.onerror = null; // prevents looping
                            currentTarget.src="/tmp/system-assets/images/no-image.png";
                        }}
                        />                    
                    <div  className="screen-message" dangerouslySetInnerHTML={{ __html: message_text }} />
                    <div  className="screen-next" dangerouslySetInnerHTML={{ __html: next_text }} />
                </div>,
            ]);
        }        
    }

}
export default Screen;
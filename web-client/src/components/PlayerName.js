import React, { Component } from 'react';
import ContentEditable from "react-contenteditable";
import {socket} from "../service/socket";


class PlayerName extends Component {

  constructor(props) {
    super(props);
    this.state = { playerName: this.props.playerName };

    console.log("player name was rendered and set to " + this.props.playerName)

  }


  handleChange = evt => {    

    const allowed_regex = /[^A-Za-z0-9]/; 
    const max_length = 20
    var new_value = evt.target.value

    if(!allowed_regex.test(new_value) && new_value.length < max_length){
      this.setState({ playerName: new_value });
      console.log("seat name changed to " + new_value)
      socket.emit("playerNameUpdate", {seatIndex: this.props.seatIndex, newName: new_value});

    } else {
      this.setState({ playerName: this.state.playerName });

    }

  };

  render() {

    var name = this.state.playerName
    if(name == null || name == ""){
      name = this.props.playerName
    }

    return (
      <ContentEditable className="name"
        html={name} // innerHTML of the editable div
        disabled={false} // use true to disable edition
        onChange={this.handleChange} // handle innerHTML change
      />      
    );
  };
}

export default PlayerName
import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { 
    faCircle,
   } from '@fortawesome/free-solid-svg-icons'

class PhaseIndicator extends Component {

    constructor(props){
        super(props);
        this.state = {count: 0};
    }

    componentDidMount() {
        this.timerID = setInterval(
          () => this.tickTimer(),
          1000
        );
    }
    
    componentWillUnmount() {
        clearInterval(this.timerID);
    }
    getClock(){
        var time = new Date();        
        return time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    }
    getTimer(){
        var date = new Date(null);
        date.setSeconds(this.state.count); 
        return date.toISOString().substr(11, 8);
    }
    tickTimer() {
        if(!this.props.tableData){
          return null;
        } else {
            if(this.props.tableData.mode == "playing-game"){
                var new_count = this.state.count + 1
                this.setState({
                    count: new_count
                });
            }else{
                this.setState({
                    count: 0
                });
          }
        }
      }

    render() {
        var table = this.props.tableData
        var current_game = table?.current_game
        var first_player = current_game?.first_player
        var timer = this.getTimer();
        var clock = this.getClock();
        var hide = false;

        var current_mode = table?.current_game?.current_mode
    
        if(current_mode == "message"){
          hide = true
        }


        if(hide){
            return null;
        }else if(!current_game || !first_player || first_player.status == null){
            return ([            
                <div className='phase-indicator blank'>
                    <div className='content'>
                    </div>
                </div>
            ]);
        } else {
            return ([            
                <div className='phase-indicator'>
                    <div className={`content color-${first_player.current_color.name} container`}>
                        <div className="col col-50">
                            First Player:<br/>
                            Round Count:<br/>
                            Game Time:<br/>
                            Clock:<br/>
                        </div>
                        <div className="col col-50 text-right">
                            <FontAwesomeIcon className="indicator" icon={faCircle} />{first_player.name}<br/>
                            {current_game.round_count}<br/>
                            {timer}<br/>
                            {clock}<br/>
                        </div>
                    </div>
                </div>
            ]);
        }
    }

}
export default PhaseIndicator;
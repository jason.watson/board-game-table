#!/bin/bash
echo "Before starting, make sure you are on the branch you'd like to build an image for, usually 'main'"
git status
echo
echo "continue?"
read wait
echo

echo "Moving to web client directory"
cd ../web-client
echo

echo "Logging into Gitlab Docker registry"
docker login registry.gitlab.com
echo

echo "Building image"
docker build -t registry.gitlab.com/jason.watson/board-game-table .
echo

echo "Pushing image"
docker push registry.gitlab.com/jason.watson/board-game-table
echo

echo "complete!"
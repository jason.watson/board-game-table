console.log("INFO: Loading Libraries and config")
const light_helpers = require("../lib/ipac_light_functions.js")
try {
    var config = require('../../config/settings.json')
} catch (error) {
    console.log("WARN: Could not load config, falling back to default config")
    var config = require('./settings.example.json')
}
// Add delay function to promise to allow
// simple delay chaining
function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
Promise.prototype.delay = function(t) {
    return this.then(function(v) {
        return delay(t, v);
    });
}

// pull pins out of the config
var all_pins = []
var b1_pins = []
var b2_pins = []
for(var i = 0; i < config.seats.length; i++){
    seat = config.seats[i]
    all_pins.push(seat.pin1)
    all_pins.push(seat.pin2)
    b1_pins.push(seat.pin1)
    b2_pins.push(seat.pin2)    
}

// define colors
var c_red = [255,0,0]
var c_green = [0,255,0]
var c_blue = [0,0,255]
var c_yellow = [255,255,0]
var c_white = [255,255,255]
var c_off = [0,0,0]



function lightTest(){
    return new Promise(function(resolve, reject){  
        console.log("INFO: starting light test")    
        Promise.resolve().then(function(){
            return light_helpers.spiralLights(all_pins, c_white, c_off, 2, 400);
        }).delay(100).then(function(){
            return light_helpers.setLights(all_pins, c_red, 100)
        }).delay(100).then(function(){
            return light_helpers.setLights(all_pins, c_green, 100)
        }).delay(100).then(function() {
            return light_helpers.setLights(all_pins, c_blue, 100)
        }).delay(100).then(function(){
            return light_helpers.setLights(all_pins, c_off, 100)
        }).then(function(){
            console.log("INFO: light test complete")
            resolve();
        });
    });
}

var network_test_counter = 0;
var network_test_max = 10
var network_test_url = "https://google.com"
function networkTest(){
    return light_helpers.flashLights(b1_pins, all_pins, c_off, c_yellow, 10, 200).then(function(){
        return new Promise(function(resolve, reject){  
            network_test_counter++;
            console.log(`INFO: Starting Network test, attempt # ${network_test_counter}/${network_test_max}`)   
                fetch(network_test_url).then(function(){
                    console.log("INFO: network is up")
                    resolve();
                }).catch(function(){
                    if(network_test_counter >= network_test_max){
                        reject(`Could not reach ${network_test_url} after ${network_test_max} attempts`)
                    } else {
                        console.log("WARN: Network test failed, retrying")
                        setTimeout(function(){
                            networkTest();
                        },5000);
                    }
                });
        }).catch(function(err_message){
            console.log(`ERROR: ${err_message}`)
            // Flash to indicate an error, then light up first seat as an error code
            return light_helpers.flashLights(b1_pins, all_pins, c_red, c_off, 10, 200).then(function(){
                return light_helpers.setTwoToneLights([all_pins[0], all_pins[1]], all_pins, c_white, c_red, 100).then(function(){
                    setTimeout(function(){
                        process.exit(1);
                    }, 1000)
                })
            })
        });
    })
}

// Main Program
Promise.resolve().then(function(){
    return light_helpers.setLights(all_pins, c_off, 100)

}).then(function(){
    return lightTest();
    
}).then(function(){
    return networkTest();

}).then(function(){    
    return light_helpers.spiralLights(b1_pins, c_green, c_off, 4, 100);

}).then(function(){    
    return light_helpers.setLights(all_pins, c_green, 100)

}).then(function(){
    console.log("INFO: table is ready")

})
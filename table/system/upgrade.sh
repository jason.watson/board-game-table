
# Grab the commit hash (or master) from the command line
COMMIT_HASH=$1
export TERM=xterm

cd ~/board-game-table

# Kill the node app that's running
echo "Closing board game table app..."
sleep 2
sudo killall node
echo

# Blank the screen
clear
figlet -f smascii12 "Upgrading Table"
echo "new version: $COMMIT_HASH"
echo

node ./table/system/set_all_lights.js 0 0 0 100
node ./table/system/set_all_lights.js 255 0 255 100

if [ -z "$COMMIT_HASH" ]
then
  echo "Skipping checkout, no hash provided"
else
  # Grab the new code
  echo "Grabbing new code from branch '$COMMIT_HASH'"
  sleep 2
  git fetch
  git checkout $COMMIT_HASH
  git pull
  echo
fi

# Move the new profile startup file into the home directory
echo "Installing new .profile file in home directory..."
sleep 2
cp ./table/system/profile ~/.profile
echo

# Install any needed node modules
echo "Installing node packages..."
cd table
sleep 2
npm install
echo

# Perform a reboot in one minute
sudo shutdown -r +1
node ./system/set_all_lights.js 0 0 0 100
node ./system/set_all_lights.js 255 255 255 100
exit 0



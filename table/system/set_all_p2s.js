const iPac = require("node-ipac");
try {
    var config = require('../../config/settings.json')
} catch (error) {
    console.log("warning, could not load config, falling back to default config")
    var config = require('./settings.example.json')
}


const args = process.argv.slice(2);
let r = args[0]
let g = args[1]
let b = args[2]

let delay_ms = 0;

if(args[3]){
    delay_ms = args[3]
}

let device = iPac();

const delay = async (ms = 1000) => new Promise(resolve => setTimeout(resolve, ms))
async function run() {
    for(var i = 0; i < config.seats.length; i++){
        var seat = config.seats[i]
    
        if(seat.pin2 < 48){
            device.setRgbLED(seat.pin2, b, g, r)
        } else {
            device.setRgbLED(seat.pin2, r, g, b)
        }
    
        await delay(delay_ms)

    }
}
  
 run ()

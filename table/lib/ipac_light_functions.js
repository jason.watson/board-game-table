const iPac = require("node-ipac");
let device = iPac();

// set a single light based on a button pin
function setLight(pin, color, delay_time){
    return new Promise(function(resolve, reject){               
        setTimeout(function(){
            if(pin < 48){
                device.setRgbLED(pin, color[2], color[1], color[0])
            } else {
                device.setRgbLED(pin, color[0], color[1], color[2])
            }  
            resolve("complete")
        }, delay_time)                         
    })
}

// set many light based on an array of button pins
function setLights(pins, color, delay_time){
    return new Promise(function(resolve, reject){       
        var promises = [];   
        for(var i = 0; i < pins.length; i++){
            var new_delay = delay_time*(i+1);
            promises.push(setLight(pins[i], color, new_delay))
        }  
        Promise.all(promises).then(function(){
            resolve("complete")
        })
    });
}

// set two different colors on the table at the same time
function setTwoToneLights(foreground_pins, background_pins, foreground_color, background_color, delay_time){
    return new Promise(function(resolve, reject){
        setTimeout(function(){
            setLights(background_pins, background_color, 0)
            setLights(foreground_pins, foreground_color, 0)
            resolve("complete")
        }, delay_time)
    })
}

//spiral a single pin around the table one color, while the rest remain another
function spiralLights(pins, foreground_color, background_color, repetitions, delay_time){
    return new Promise(function(resolve, reject){       
        var promises = [];   
        for(var reps = 0; reps < repetitions; reps ++){
            for(var i = 0; i < pins.length; i++){
                var foreground_pin = pins[i]
                var background_pins = pins.filter(function(item) {
                    return item !== foreground_pin
                })
                var new_delay = delay_time*(i+1 + reps*pins.length);           
                promises.push(
                    setTwoToneLights([foreground_pin], background_pins, foreground_color, background_color, new_delay)
                )                
            }  
        }
        Promise.all(promises).then(function(){
            resolve("complete")
        })
    });
}

function flashLights(foreground_pins, background_pins, foreground_color, background_color, repetitions, delay_time){
    return new Promise(function(resolve, reject){       
        var promises = [];   
        var flash_number = 0
        for(var reps = 0; reps < repetitions; reps ++){            
            // turn foreground "off" by setting all lights to the background color
            flash_number += 1;
            promises.push(
                setTwoToneLights(foreground_pins, background_pins, background_color, background_color, delay_time*(flash_number))
            )    
            // turn the foreground lights "on" by setting them to the foreground color
            flash_number += 1;
            promises.push(
                setTwoToneLights(foreground_pins, background_pins, foreground_color, background_color, delay_time*(flash_number))
            )                                               
        }                
        // turn foreground "off" by setting all lights to the background color
        flash_number += 1;
        promises.push(
            setTwoToneLights(foreground_pins, background_pins, background_color, background_color, delay_time*(flash_number))
        )   
        Promise.all(promises).then(function(){
            resolve("complete")
        })
    });
}

module.exports = { flashLights, spiralLights, setTwoToneLights, setLights, setLight}
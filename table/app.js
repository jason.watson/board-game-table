// Include libraries
var Logger = require('./src/logger')
var Seat = require('./src/seat');
var Game = require('./src/game');
var Screen = require('./src/screen');
var Speaker = require('./src/speaker');
var Exec = require('./src/exec');
var Table = require('./src/table');
var Web = require('./src/web');
var Keyboard = require('./src/keyboard');
var Webhook = require('./src/webhook');
var FileSystem = require('fs')


// Load Config
global.config_folder = "../config"
try {
    global.config = require(`${global.config_folder}/settings.json`)
} catch (error) {
    console.log("warning, could not load config, falling back to default config")
    global.config = require('./settings.example.json')
}
global.config.games_folder = `${global.config_folder}/games`



// Parse Params
const args = process.argv.slice(2);

// Setup args
if(args.includes("-h")){
    console.log()
    console.log("-h     Print this message and exit")
    console.log("-a     Enable audio")
    console.log("-d     Enable display")
    console.log("-s     Enable scripts")
    console.log("-b     Enable button LEDs")
    console.log("-w     Enable web interface")
    console.log("-e     Enable webhook interface")
    console.log("-k     Enable keyboard interface")
    console.log("-c     Write log messages to console")
    console.log("-l     Write log messages to log file")
    console.log("-v     Show debug log messages")
    console.log()
    console.log("usage: node app.js [options]")
    console.log()
    console.log("args: " + args)
    console.log()
    process.exit()
}

speaker_enabled = args.includes("-a");
exec_enabled = args.includes("-s")
leds_enabled = args.includes("-b")
screen_enabled = args.includes("-d")
web_enabled = args.includes("-w")
webhook_enabled = args.includes("-e")
keyboard_enabled = args.includes("-k")

// Setup Logging
// error - something has failed
// warn -  something bad is happening
// info -  something is happening
// debug - how something is happening



var logger = new Logger(global.config.log_file);
console.error = function (msg) {
    logger.consoleMsg("error", msg) ;
    logger.logMsg("error", msg);
};
console.warn = function (msg) {
    if(args.includes("-c")){logger.consoleMsg("warn", msg)}
    if(args.includes("-l")){logger.logMsg("warn", msg)}
};
console.info = function (msg) {
    if(args.includes("-c")){logger.consoleMsg("info", msg)}
    if(args.includes("-l")){logger.logMsg("info", msg)}
};
console.debug = function (msg) {
    if(args.includes("-v")){
        if(args.includes("-c")){logger.consoleMsg("debug", msg)}
        if(args.includes("-l")){logger.logMsg("debug", msg)}
    }
};


// Load Games from games directory
var games = [];
const dir = FileSystem.opendirSync("../config/games")
var sub_directory;
while ((sub_directory = dir.readSync()) !== null) {
    var sub_directory_name = sub_directory.name
    if(!sub_directory_name.startsWith("!")){
        try{
            let game_config = FileSystem.readFileSync(`../config/games/${sub_directory_name}/config.json`);
            games.push(new Game(JSON.parse(game_config)))
            console.info("Loaded game " + sub_directory_name)
        } catch(err){
            console.warn("Failed to load game: " + sub_directory_name)
        }
    }
}
dir.closeSync()
games.sort(function(a, b) {
    var keyA = a.getOrder(), keyB = b.getOrder();
    if (keyA < keyB) return -1;
    if (keyA > keyB) return 1;
    return 0;
});

// Load Seats from config file
var seats = [];
for(var i = 0; i < global.config.seats.length; i++){
    var seat = global.config.seats[i]
    seats.push(new Seat(seat.id, seat.key1, seat.key2, seat.key3, seat.key4, seat.pin1, seat.pin2, leds_enabled))
}

// Load Exec
var exec = new Exec(exec_enabled);

// Load Exec
var webhook = new Webhook(webhook_enabled);

// Load Screen
var screen = new Screen(screen_enabled, exec, global.config.screen_width, global.config.screen_height, global.config.point_size);


// Load Speaker
var speaker = new Speaker(speaker_enabled, exec);

// Build Table
let table = new Table(seats, games, screen, speaker, exec, webhook);
console.info(`Configured ${seats.length} table seats and ${games.length} games`)


table.start();

// -----------------------
// Setup web Interface
// -----------------------
let web = new Web(web_enabled, global.config.server_address, global.config.server_port, global.config.client_address, global.config.client_port, table)
web.start();

speaker.setWeb(web);
speaker.playSystemSound("startup", 80)

// -----------------------
// Setup Keyboard Interface
// -----------------------
let keyboard = new Keyboard(keyboard_enabled, table)
keyboard.start();

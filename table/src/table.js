module.exports = table

function table(seat_array, game_array, screen, speaker, exec, webhook) {

  // Save Args
  var self = this
  var _seats = seat_array 
  var _screen = screen
  var _speaker = speaker
  var _exec = exec
  var _webhook = webhook
  var _games = game_array

  // Setup Variables
  var _game_index;  
  var _current_game;
  var _p1;
  var _mode;

  var _current_game_mode_index = 0;

  var _button_press_callback = function(){};

  // Actions
  this.start = function(){
    console.debug(`table action: start`);

    webhook.put(global.config.webhook_startup);
   
    _mode = "idle";

    _game_index=-1;  
    _setting_index=-1;  
    _current_game = _games[0];

    _p1 = _seats[0]
    for(seat of _seats){
      seat.clearStatus();
      seat.clearColor();
      seat.LEDOff();
    }
    _screen.showIdleInstructions();



  }
  this.reset = function(){
    console.debug(`table action: reset`);
    webhook.put(global.config.webhook_reset);    

    _mode = "idle";

    _game_index=-1;  
    _current_game.reset();
    _current_game = _games[0];

    _p1 = _seats[0]
    for(seat of _seats){
      seat.clearStatus();
      seat.clearColor();
      seat.LEDOff();
    }
    _screen.showIdleInstructions();
    _button_press_callback();

  }
  this.processButtonPress = function (key = "") {
    console.debug(`table action: processButtonPress(${key})`);

    var b = key;
    var p1b1 = _p1.getB1();
    var p1b2 = _p1.getB2();


    if(key == _p1.getB4()){
      self.reset();
    }

    // IDLE
    if (_mode == "idle"){
      if(b == p1b1){
        _mode = "choosing-game"
      } else {
        _screen.showIdleInstructions();
      }
    }


   
    // CHANGING SETTINGS / CHOOSING GAME
    if(_mode == "changing-settings" || _mode == "choosing-game"){
      if(b == p1b1){

        console.debug("_setting_index = " + _setting_index)
        console.debug("_game_index = " + _game_index)

        // check if we need to change modes
        if(_setting_index >= 0){
          // if more settings are added in the future
          // the boundry above will need to be increased
          _game_index = -1;
          _setting_index = -1
          _mode = "choosing-game";

        } else if (_game_index == _games.length - 1) {
          _setting_index = -1
          _game_index = -1
          _mode = "changing-settings"
        }

        // process the mode
        if(_mode == "changing-settings"){
          _setting_index +=1;
          _speaker.playSystemSound("game_next", 80)
          if(_setting_index == 0){
            webhook.put(global.config.webhook_select_shutdown);
            _screen.showShutdownInstructions();
            _p1.LEDAdminB1Available();
            _p1.LEDAdminB2Danger();
          }
         
        }
        if(_mode == "choosing-game"){
          incrementGame();
          _speaker.playSystemSound("game_next", 80)
          _screen.showImage(`${global.config.games_folder}/${_current_game.getConfig().id}/${_current_game.getConfig().select_image}`);


          _webhook.put(_current_game.getConfig().webhook_select)
          _p1.LEDAdminB1Available();
          _p1.LEDAdminB2Available();
        }

     
      }else if(b == p1b2){
        if(_mode == "changing-settings"){
          if(_setting_index == 0){
            _speaker.playSystemSound("select", 80)
            _screen.showShuttingDown();
            _mode = "shutting-down"
            webhook.put(global.config.webhook_shutdown);
            _exec.run("sleep 10 && sudo halt");
          }
          // Future settings go here, an index for each
        }

        if(_mode == "choosing-game"){
          if(_current_game.getConfig().modes != null && _current_game.getConfig().modes.length > 1){
            _mode = "choosing-game-mode"
          } else {
            _mode = "adding-players"
            _p1.LEDOff();            
          }
          _speaker.playSystemSound("select", 80)
        }

      }
      b = "";
    }


    // CHOOSING GAME MODE
    if (_mode ==  "choosing-game-mode"){
      if(b == p1b1){        
        _current_game.incrementCurrentGameMode();
        _screen.showImage(_current_game.getCurrentGameMode().image);
        _speaker.playSystemSound("select", 80)

      }else if(b == p1b2){
        _mode = "adding-players"
        _p1.LEDOff();
        _speaker.playSystemSound("select", 80)
      }

    }
  
    // ADDING PLAYERS
    if (_mode ==  "adding-players"){
      if(b == p1b2){
        if(self.getFilledSeats().length > 0){                  
            _mode = "choosing-first-player"

        } else {
          _speaker.playSystemSound("error", 80)
        }
      } else {          
        for (const seat of _seats) {
          if(b == seat.getB1()){
            seat.incrementColor();
            seat.setStatus("in");
          }
          _p1.LEDAdminB2Available();
        }
        _screen.showAddPlayersInstructions()
      }
     
    
    // CHOOSING FIRST PLAYER
    } else if (_mode == "choosing-first-player"){
      _screen.showChooseFirstPlayerInstructions();
      
      if(b == p1b2){
        var fs = self.getFilledSeats()
        _mode = "playing-game"
        _current_game.start(self,fs[Math.floor(Math.random()*fs.length)]);

      } else {
        for (const seat of _seats) {
          if(b == seat.getB1()){
            _mode = "playing-game"
            _current_game.start(self,seat);
          }        
        }
      }
      
    
    // PLAYING GAME
    } else if (_mode =="playing-game"){
      _current_game.processButtonPress(b);

    }

    _button_press_callback();

  }

  // Properties
  this.getMode = function(){return _mode;}
  this.getGames = function(){return _games;}
  this.getGame = function(){return _current_game;}
  this.getScreen = function(){return _screen;}
  this.getSpeaker = function(){return _speaker;}
  this.getExec = function(){return _exec;}
  this.getWebhook = function(){return _webhook;}
  this.getP1 = function(){return _p1;}
  this.getP = function(seat_index){
    return _seats[seat_index]
  }

  this.setButtonPressCallback = function(callback){_button_press_callback = callback};

  this.getSeats = function(){return _seats;}
  this.getSeatData = function(){
    var data = []
    for(var i = 0; i < _seats.length; i++){
        data.push(_seats[i].toData());
    }   
    return data;    
  }
  this.getFilledSeats =  function() {
    var filled_seats = []
    for (const seat of _seats) {
      if (seat.isFilled()) {
       filled_seats.push(seat);
      }
    }
    return filled_seats;
  }

  // Display
  this.toString = function () {
    var output = "";
    for (var i = 0; i < _seats.length; i++) {
      output += `Seat #${i+1}:\t\t${_seats[i].toString()}`
      output += "\n";
    }
    return output;
  }

  this.toData = function (){
    return {
      current_game_index: _game_index,
      setting_index: _setting_index,
      current_game: _current_game.toData(),
      seats: self.getSeatData(),
      player_one: _p1.toData(),
      mode: _mode
    }
  }

  // Private
  function incrementGame() {
    if (_game_index == _games.length - 1) {
      _game_index = 0;
    } else {
      _game_index += 1;
    }
    _current_game = _games[_game_index];
  }
 
}
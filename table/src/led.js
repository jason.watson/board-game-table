module.exports = led
const iPac = require("node-ipac");
let device = iPac();

function led(pin, enabled){

    var _enabled = enabled
    var self = this;
    var _pin = pin;
    var _color = { name: null, red: 0, green: 0, blue: 0 }

  
    this.setColor = function(color){
       _color = color
    }
   

    var pulseInterval = null;
    this.pulse = function(min_brightness, max_brightness){
        var brightness = 0
        var direction = "up"
        var steps = 0.15;

        self.turnOn(brightness);
        pulseInterval = setInterval(function(){
            if(direction == "down"){
                brightness = brightness - steps
            } else {
                brightness = brightness + steps
            }
            if(brightness - steps < min_brightness){
                direction = "up"
            }
            if(brightness + steps > max_brightness){
                direction = "down"
            }
            self.applyColor(brightness);
        }, 100);

    }

    var alertInterval = null;
    this.alert = function(fade_min_brightness, fade_max_brightness){
        var brightness = 0
        var direction = "up"

        var fade_steps = 0.15;
        var alert_blinks = 20

        self.turnOn(brightness);
        alertInterval = setInterval(function(){
            alert_blinks = alert_blinks - 1
            
            if(alert_blinks > 0){
                steps = 1
                min_brightness = 0
                max_brightness = 1
            } else {
                steps = fade_steps
                min_brightness = fade_min_brightness
                max_brightness = fade_max_brightness
                alert_blinks = 0;
            }

            if(direction == "down"){
                brightness = brightness - steps
            } else {
                brightness = brightness + steps
            }
            if(brightness - steps < min_brightness){
                direction = "up"
            }
            if(brightness + steps > max_brightness){
                direction = "down"
            }
            self.applyColor(brightness);
        }, 100);

    }



    this.turnOn = function(brightness){
        clearInterval(pulseInterval)
        clearInterval(alertInterval)
        this.applyColor(brightness);
    }
    this.turnOff = function(){
        clearInterval(pulseInterval)
        clearInterval(alertInterval)        
        this.applyColor(0);
    }


    // Private
    this.applyColor = function (brightness){
        var c = _color

        if (_pin > 48) {
            var r = c.blue
            var g = c.green
            var b = c.red
        } else {
            var b = c.blue
            var g = c.green
            var r = c.red
        } 

        b = b*brightness
        g = g*brightness
        r = r*brightness


        if(enabled){
            try{
                device.setRgbLED(_pin, b, g, r)
            } catch(err){
                console.error("could not reach LED on pin "+_pin)
                console.error(err.toString())
            }
        } else {
            //console.debug(`led: setting ${_pin} to R:${c.red} G:${c.green} B:${c.blue} brightness:${brightness}`)

            
          // LEDs are updated so frequently that logs will overload
          // if a message is shown here, turn on if you really need it
        }
       
    }

}




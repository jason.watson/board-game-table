module.exports = exec
const _exec = require("child_process").exec;
var _kill  = require('tree-kill');

function exec(exec_enabled = true){

    var _self = this
    var _enabled = true;
    var _exec_enabled = exec_enabled;
    var _pids = [];
    var _script_pid = null;

    this.runScript = function(script_path){
        console.debug(`exec: running script ->  ${global.config_folder}/game-scripts-common.sh ${script_path}`)
        if(_exec_enabled){
            _self.killAll();
            var child = _exec(`bash -c "$(awk 'FNR==1{print ""}1' ${global.config_folder}/game-scripts-common.sh ${script_path})"`)   
            _script_pid = child.pid
            _pids.push(child.pid)
        } else {
            console.warn("exec scripts are disabled, skipping script")
        }
    }

    this.killScript = function(){
        if(_script_pid != null){
            _kill(_script_pid)
            _script_pid = null;
        }
    }

    this.run = function(command){
        console.debug(`exec: running command ->  ${command}`)
        if(_enabled){
            var child = _exec(command);
            _pids.push(child.pid)
        } else {
            console.warn("exec is disabled, skipping command")
        }
        
    }

    this.killAll = function(){
        for(var i =0; i < _pids.length; i++){
            console.debug(`exec: kill PID ${_pids[i]}`)
            _kill(_pids[i]);
        }     
        _self.killScript();
        _pids = []   
    }

}
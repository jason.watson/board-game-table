module.exports = speaker

function speaker(enabled = true, exec) {

  var _enabled = enabled;
  var _exec = exec;
  var _web = null;

  this.setWeb = function(web){
    _web = web
  }

  this.playSystemSound = function(sound_name, volume){
    console.info(`speaker: playing system sound ${sound_name}`)
    this.play(sound_name, `${global.config_folder}/system-assets/audio/${sound_name}.wav`,volume)
  }

  this.play = function(sound_name,filename, volume){
    console.info(`speaker: playing sound ${filename} at volume ${volume.toString()}`)

    var volume_command = `amixer sset -q -M 'Headphone' ${volume.toString()}%`
    var play_command = `aplay ${filename}`

    _web.sendSoundToFrontend(sound_name)

    if(_enabled){
      exec.run(volume_command , (error, stdout, stderr) => {
        if (error) {
          console.error(`speaker: error setting volume to '${volume}'%, msg: ${error.message}`);
          return;
        }
      });
      exec.run(play_command, (error, stdout, stderr) => {
        if (error) {
          console.error(`speaker: error playing sound '${filename}', msg: ${error.message}`);
          return;
        }
      });
    } else {
      console.warn(`speaker: audio is disabled, could not set volume with ${volume_command} or play sound with ${play_command}`)
    }
      
  }

  this.stop = function(){
    exec.run("killall aplay")
  }

}
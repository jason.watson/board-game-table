module.exports = game

function game(config){

    var self = this;
    var _config = config;

    
    var _sequence = []
    var _phase_index = 0;
    var _current_phase;
    var _current_mode;
    var _previous_mode;

    // the mode for the game, i.e. hard mode, easy mode, short game, long game, etc.
    // not to be confused with the "mode" from the phase check
    var _current_game_mode;
    var _current_game_mode_index = 0;
    if(_config.modes == null || _config.modes.length == 0){ 
        _current_game_mode = {name: "normal-mode", image: ""}
    } else {
        _current_game_mode = _config.modes[0]
    }
        

    var _seats
    var _screen;
    var _speaker;
    var _table;
    var _exec;
    var _webhook;
    var _p1;

    var _round_count;

    var _first_player;
    var _first_player_index;




    // actions
    this.reset = function(){
        var _current_game_mode_index = 0;
        if(_config.modes == null || _config.modes.length == 0){ 
            _current_game_mode = {name: "normal-mode", image: ""}
        } else {
            _current_game_mode = _config.modes[0]
        }
        var _phase_index = 0;
        var _sequence = []
        _current_phase = null;
        _current_mode = "start"

    }
    this.start = function(table, forced_first_player_seat = null){
        _table = table
        _seats = table.getFilledSeats()
        _screen = table.getScreen();
        _speaker = table.getSpeaker();
        _exec = table.getExec();
        _webhook = table.getWebhook();
        _sequence = config.sequence;            
        _p1 = table.getP1();

       
        if(forced_first_player_seat == null){            
            forced_first_player_seat = _p1
        }


        _first_player = forced_first_player_seat
        _first_player_index = _seats.indexOf(_first_player)

        _round_count = 0;

        _phase_index = -1;
        _current_phase = null;
        _current_mode = "start"
        _previous_mode = "start"

        for (const seat of _seats) {
            seat.LEDOff();
            seat.clearStatus();
        }

        executePhaseChecks();
    }
    

    this.processButtonPress = function(key){
        console.debug(`game action: processButtonPress(${key})`)
        console.debug(`_current_mode == ${_current_mode}`)

       executePhaseChecks(key);
    }

    // properties
    this.getID = function(){
        return _config.id;
    }
    this.getName = function(){
        return _config.name;
    }
    this.getCurrentGameMode = function(){       
        return _current_game_mode;
    }   
    this.incrementCurrentGameMode = function(){
        if(_config.modes == null || _config.modes.length == 0){            
            _current_game_mode_index = 0

        } else if (_config.modes.length == 1){
            _current_game_mode = _config.modes[0]
            _current_game_mode_index = 0

        } else {
            if (_current_game_mode_index == _config.modes.length - 1) {
                _current_game_mode_index = 0;            
            } else {
                _current_game_mode_index += 1;            
            }
            _current_game_mode = _config.modes[_current_game_mode_index]
        }
    }
    
    this.getOrder = function(){
        return _config.order || 99999;
    }
    this.getConfig = function(){
        return _config;
    }
    this.getCurrentPhase = function(){
        return _current_phase;
    }
   
    this.toData = function(){
        return {
            id: _config.id,            
            config: _config,
            name: _config.name,
            current_mode: _current_mode,
            current_game_mode: _current_game_mode,            
            current_phase: _current_phase,
            current_phase_index: _phase_index,
            first_player: _first_player?.toData(),
            first_player_index: _first_player_index,
            round_count: _round_count
        }
    }


// TODO:
// stop executing button presses if we're in the end state
// i.e. when helicopter buttons light up red, pressing a button should not 
// revert them to their idle states



    // private 
    function executePhaseChecks(key = ""){
        var b = key;
        var p1b1 = _p1.getB1();
        var p1b2 = _p1.getB2();

        // Special actions
        // B3 removes players from the game
        for(var i = 0; i < _seats.length; i++){
            seat = _seats[i];
            if(b == seat.getB3()){
                seat.setStatus("dead");
            }
        }
        // START
        if(_current_mode == "start"){
            _speaker.playSystemSound("round_next", 80)
            _round_count += 1;
            incrementPhase();
            executePhaseChecks();
            return;
        }

        // Skip phases that shouldn't be run on this round
        if(_current_phase.only_in_modes && !_current_phase.only_in_modes.includes(_current_game_mode.name) ){
            console.debug(`Skipping phase ${_current_phase.mode} due to a only_in_mode clause (${_current_game_mode.name})`)
            incrementPhase();
            executePhaseChecks();

        }else if(_current_phase.not_in_modes && _current_phase.not_in_modes.includes(_current_game_mode.name)){
            console.debug(`Skipping phase ${_current_phase.mode} due to a not_in_mode clause (${_current_game_mode.name})`)
            incrementPhase();
            executePhaseChecks();

        }else if(_current_phase.only_on_rounds && !_current_phase.only_on_rounds.includes(_round_count)){
            console.debug(`Skipping phase ${_current_phase.mode} due to a only_on_rounds clause`)
            incrementPhase();
            executePhaseChecks();

        }else if(_current_phase.not_on_rounds && _current_phase.not_on_rounds.includes(_round_count)){
            console.debug(`Skipping phase ${_current_phase.mode} due to a not_on_rounds clause`)
            incrementPhase();
            executePhaseChecks();

        }else if(_current_phase.only_with_player_counts && !_current_phase.only_with_player_counts.includes(_seats.length)){
            console.debug(`Skipping phase ${_current_phase.mode} due to a only_with_player_counts clause`)
            incrementPhase();
            executePhaseChecks();

        }else if(_current_phase.not_with_player_counts && !current_phase.not_with_player_counts.includes(_seats.length)){
            console.debug(`Skipping phase ${_current_phase.mode} due to a not_with_player_counts clause`)
            incrementPhase();
            executePhaseChecks();

        } else {


            // Client config changes
            if(_current_mode == "update_config"){
                console.info(`dynamically updating config`)
                console.log(_current_phase.config)

                Object.keys(_current_phase.config).forEach((key, index) => {
                    _config[key] = _current_phase.config[key];
                });
                incrementPhase();
                executePhaseChecks();
            }    


            // WEBHOOK
            if(_current_mode == "webhook"){
                _webhook.put(_current_phase.webhook)
                incrementPhase();
                executePhaseChecks();
            }      
            

            // END
            if(_current_mode == "end"){
                if(b == p1b2){
                    _table.reset();
                } else {
                    if(_current_phase.hide_tracker){
                        _screen.showImage(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`)
                    } else {
                        _screen.showImageWithTracker(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`, _first_player.getColor().name, _round_count)
                    }
                    _seats.forEach(function(seat){
                        if(seat.getStatus() != "dead"){
                            seat.setStatus("passed");
                        }
                    });
                    console.info("end phase: " + _current_phase.comment);
                    console.info("first player: " + _first_player.getName());                    
                }
            }

            // Message Screen
            if(_current_mode == "message"){
                if(b == p1b2){
                    incrementPhase();
                    executePhaseChecks();
                    if(_previous_mode != "start"){
                        _speaker.playSystemSound("phase_next", 80)    
                    }
                    return;
                } else {
                    if(_current_phase.hide_tracker){
                        _screen.showImage(`${global.config_folder}/system-assets/images/wait.png`);        
                    }
                    _seats.forEach(function(seat){
                        if(seat.getStatus() != "dead"){
                            seat.setStatus("passed");
                        }
                    });
                    _p1.LEDAdminB2Available();
                    console.info("upkeep/info/video phase: " + _current_phase.comment);
                    console.info("first player: " + _first_player.getName());                    
                }
            } 



            // UPKEEP
            if(_current_mode == "upkeep" || _current_mode == "info"){
                if(b == p1b2){
                    incrementPhase();
                    executePhaseChecks();
                    if(_previous_mode != "start"){
                        _speaker.playSystemSound("phase_next", 80)    
                    }
                    return;
                } else {
                    if(_current_phase.hide_tracker){
                        _screen.showImage(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`)
                    } else {
                        _screen.showImageWithTracker(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`, _first_player.getColor().name, _round_count)
                    }
                    _seats.forEach(function(seat){
                        if(seat.getStatus() != "dead"){
                            seat.setStatus("passed");
                        }
                    });
                    _p1.LEDAdminB2Available();
                    console.info("upkeep/info/video phase: " + _current_phase.comment);
                    console.info("first player: " + _first_player.getName());
                    
                }
            } 

            // PRIME FINITE ROUND
            if(_current_mode == "prime-finite-round"){
                _seats.forEach(function(seat){
                    if(seat.getStatus() != "dead"){
                        seat.setStatus("in")
                    }
                });
                _first_player.setStatus("active-passable");
                incrementPhase();
                executePhaseChecks();
                return;
            }

            // FINITE ROUND
            if(_current_mode == "finite-round"){
                _seats.forEach(function(seat){
                    if(seat.getStatus() != "passed"){
                        if( b == seat.getB1()){
                            if(seat.getStatus() == "active-passable" ){
                                replaceAllSeatsStatus("active-passable", "in");
                                giveStatusToNextSeat(seat, "active-passable");
                                _speaker.playSystemSound("player_next", 80)
                            } else {
                                replaceAllSeatsStatus("active-passable", "in");
                                _speaker.playSystemSound("player_jump", 80)
                                seat.setStatus("active-passable");
                            } 
                        }
                        if(b == seat.getB2()){
                            if(seat.getStatus() == "active-passable" ){
                                replaceAllSeatsStatus("active-passable", "in");
                                giveStatusToNextSeat(seat, "active-passable");
                                _speaker.playSystemSound("player_next", 80)
                                seat.setStatus("passed");
                            }
                        }
                    }        
                })
                if(allSeatsHaveStatus("passed")){
                    _seats.forEach(function(seat){
                        if(seat.getStatus() != "dead"){
                            seat.clearStatus()
                        }
                    });           
                    incrementPhase();
                    executePhaseChecks();
                    if(_previous_mode != "start"){
                        _speaker.playSystemSound("phase_next", 80)    
                    }
                    return;
                } else {
                    if(_current_phase.hide_tracker){
                        _screen.showImage(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`)
                    } else {
                        _screen.showImageWithTracker(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`, _first_player.getColor().name, _round_count)
                    }                
                    console.info("finite round phase: " + _current_phase.comment);
                    console.info("first player: " + _first_player.getName());
                        
                }     
            } 

            // PRIME INFINITE ROUND
            if(_current_mode == "prime-infinite-round"){       
                _seats.forEach(function(seat){
                    if(seat.getStatus() != "dead"){
                        seat.setStatus("in")
                    }
                });    
                _first_player.setStatus("active");
                incrementPhase();
                executePhaseChecks();
                return;
            }

            // INFINITE ROUND
            if(_current_mode == "infinite-round"){
                _seats.forEach(function(seat){
                    if( b == seat.getB1()){
                        if(seat.getStatus() == "active" ){
                            replaceAllSeatsStatus("active", "in");
                            giveStatusToNextSeat(seat, "active");
                            _speaker.playSystemSound("player_next", 80)
                        } else {
                            replaceAllSeatsStatus("active", "in");
                            seat.setStatus("active");
                            _speaker.playSystemSound("player_jump", 80)
                        } 
                    }
                })         
                if(_current_phase.hide_tracker){
                    _screen.showImage(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`)
                } else {
                    _screen.showImageWithTracker(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`, _first_player.getColor().name, _round_count)
                }            
                console.info("infinite round phase: " + _current_phase.comment);
                console.info("first player: " + _first_player.getName());
                            
            }
        
            // PRIME ONE SHOT
            if(_current_mode == "prime-one-shot"){
                _seats.forEach(function(seat){
                    if(seat.getStatus() != "dead"){
                        seat.setStatus("in");
                    }
                });
                incrementPhase();
                executePhaseChecks();
                return;
            }

            // ONE SHOT
            if(_current_mode == "one-shot"){
                _seats.forEach(function(seat){
                    if(b == seat.getB1() && seat.getStatus() != "passed"){
                        seat.setStatus("passed")
                        if(!allSeatsHaveStatus("passed")){
                            _speaker.playSystemSound("player_next", 80)
                        }
                    }
                })

                if(allSeatsHaveStatus("passed")){
                    _seats.forEach(function(seat){
                        if(seat.getStatus() != "dead"){
                            seat.clearStatus()
                        }
                    });
                    incrementPhase();                   
                    executePhaseChecks();
                    if(_previous_mode != "start"){
                        _speaker.playSystemSound("phase_next", 80)    
                    }
                    return;
                } else { 
                    if(_current_phase.hide_tracker){
                        _screen.showImage(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`)
                    } else {
                        _screen.showImageWithTracker(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`, _first_player.getColor().name, _round_count)
                    }                
                    console.info("oneshot phase: " + _current_phase.comment);
                    console.info("first player: " + _first_player.getName());
                    
                }
            }

            // PRIME ONE SHOT WITH ORDER
            if(_current_mode == "prime-one-shot-with-order"){
                _seats.forEach(function(seat){
                    if(seat.getStatus() != "dead"){
                        seat.setStatus("in");
                    }
                });
                _first_player.setStatus("active");
                incrementPhase();
                executePhaseChecks();
                return;
            }

            // ONE SHOT WITH ORDER
            if(_current_mode == "one-shot-with-order"){

                var last_seat = null
                _seats.forEach(function(seat){
                    if(b == seat.getB1() && seat.getStatus() == "active"){
                        last_seat = seat
                        giveStatusToNextSeat(seat, "active");
                        if(!allSeatsHaveStatus("passed")){
                            _speaker.playSystemSound("player_next", 80)
                        }
                    }
                })
                if(last_seat != null){
                    last_seat.setStatus("passed")
                }

                if(allSeatsHaveStatus("passed")){
                    _seats.forEach(function(seat){
                        if(seat.getStatus() != "dead"){
                            seat.clearStatus()
                        }
                    });
                    incrementPhase();                   
                    executePhaseChecks();
                    if(_previous_mode != "start"){
                        _speaker.playSystemSound("phase_next", 80)    
                    }
                    return;
                } else { 
                    if(_current_phase.hide_tracker){
                        _screen.showImage(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`)
                    } else {
                        _screen.showImageWithTracker(`${global.config.games_folder}/${_config.id}/${_config.image_folder}/${_current_phase.image}`, _first_player.getColor().name, _round_count)
                    }                
                    console.info("oneshot-with-order phase: " + _current_phase.comment);
                    console.info("first player: " + _first_player.getName());
                    
                }
            }

            // MOVE FIRST PLAYER TOKEN             
            if(_current_mode == "move-first-player-token"){
                incrementFirstPlayerToken();
                incrementPhase();
                executePhaseChecks();
                return;
            }
        }

    }
    function incrementPhase() {
        if (_phase_index == null || _phase_index == _sequence.length - 1) {
            _phase_index = -1;
            _current_phase =  null
            _current_mode = "start"
        } else {
            _phase_index += 1;
            _current_phase = _sequence[_phase_index];
            _previous_mode = _current_mode;
            _current_mode = _current_phase.mode
        }
 
    }  
    function incrementFirstPlayerToken() {
        if (_first_player_index == null || _first_player_index == _seats.length - 1) {
            _first_player_index = 0;
        } else {
            _first_player_index += 1;
        }
        _first_player = _seats[_first_player_index];
        if(_first_player.getStatus() == "dead"){
            incrementFirstPlayerToken();
        }
    }   
    function allSeatsHaveStatus(status){
       for(const s of _seats){  
         if(s.getStatus() != "dead"){
           if(s.getStatus() != status){
            return false;
           }
        }
       }
       return true;
    }
    function giveStatusToNextSeat(seat, status){
        var unpassed_seats = []

        for(const s of _seats){
            if(s.getStatus() != "passed" && s.getStatus() != "dead"){
                unpassed_seats.push(s);
            }
        }

        var seat_index = unpassed_seats.indexOf(seat)
        if(seat_index == unpassed_seats.length -1){
            unpassed_seats[0].setStatus(status);
        } else {
            unpassed_seats[seat_index+1].setStatus(status);
        }
    }
    function replaceAllSeatsStatus(status, new_status=null){
        var unpassed_seats = []

        for(const s of _seats){
            if(s.getStatus() != "passed" && s.getStatus() != "dead"){
                unpassed_seats.push(s);
            }
        }

        for(const s of unpassed_seats){
            if(s.getStatus() == status){
                s.setStatus(new_status)
            }
        }
    }
  

}




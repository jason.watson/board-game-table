module.exports = webhook
var request = require('request');


function webhook(webhook_enabled = true){

    var _self = this
    var _enabled = true;
    var _webhook_enabled = webhook_enabled;
    
    this.put= function(webhook_url){

        if(typeof(webhook_url) != "undefined" && webhook_url != ""){

            console.debug(`webhook: firing webhook ->  ${global.config.webhook_prefix}${webhook_url}`)

            if(_webhook_enabled){
                fetch(`${global.config.webhook_prefix}${webhook_url}`, {
                    method: "PUT",
                    headers: { "Content-Type": "application/json"},
                    body: JSON.stringify({foo: "bar"})        
                }).then((response) => response.text())
                .then((body) => {
                    // console.log(body);
                }).catch((error) => {
                    console.error('webhook failed to send', error);
                }); 
            } else {
                console.warn("webhooks are disabled, skipping webhook")

            }
        } else {
            console.warn("webhook is not defined, skipping webhook")
        }
    }


}
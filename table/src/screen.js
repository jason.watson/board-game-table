module.exports = screen
const fs = require('fs')

function screen(enabled, exec, screen_width, screen_height, point_size) {

    var self = this;
    var _last_exec_command = "";
    var _enabled = enabled
    var _exec = exec

    this.turnOff = function(){
        console.info("screen: Turning Off")
        exec.run("clear")
    }
    this.showIdleInstructions = function(){
        console.info("screen: Press P1B1 to select a game")
        self.showImage(`${global.config_folder}/system-assets/images/idle.png`);
    }
    this.showAddPlayersInstructions = function(){
        console.info(`screen: Press your B1 to choose your color. press P1B2 to start the game.`)
        self.showImage(`${global.config_folder}/system-assets/images/add-players.png`);        
    }
    this.showChooseFirstPlayerInstructions = function(){
        console.info(`screen: Press the B1 of whichever player should be the first player`)
        self.showImage(`${global.config_folder}/system-assets/images/select-first-player.png`);        
    }
    this.showShutdownInstructions = function(){
        console.info(`screen: Press P1B2 to shutdown the table, P1B! to choose a game`)
        self.showImage(`${global.config_folder}/system-assets/images/shutdown.png`);        
    }
    this.showShuttingDown = function(){
        console.info(`screen: System is shutting down`)
        self.showImage(`${global.config_folder}/system-assets/images/shutting-down.png`);        
    }
    this.showJammed = function(){
        console.info(`screen: System is jam, clear jam to continue`)
        self.showImage(`${global.config_folder}/system-assets/images/jammed.png`, false);        
    }


    this.showImageWithTracker = function(filepath, tracker_color, tracker_value){  
        console.info(`screen: showing image ${filepath} with tracker (color: ${tracker_color}, value: ${tracker_value})`)

        var gravity = "northeast"
        var font = "NimbusMonoPS-Bold"
        var x = point_size / 4;
        var y = point_size - x;
        var tracker_name = tracker_color.toUpperCase();

        if(fs.existsSync(filepath)){
            var command = `convert ${filepath} -resize ${screen_width}x${screen_height}! ${global.config_folder}/system-assets/images/tracker.png -resize ${screen_width}x${screen_height}! -flatten -gravity ${gravity} -pointsize ${point_size} -fill ${tracker_color} -stroke none -strokewidth 2 -font ${font} -annotate +${x}+${y} '${tracker_value}\n\n${tracker_name}' bgra:/dev/fb0`
            if(_enabled){
                exec.run(command);
                _last_exec_command = command;
                console.debug(`screen: command ran was ${_last_exec_command}`)
            } else {
                console.warn(`screen: screen is disabled, would have ran: ${command}`)
            }
        } else {
            console.error(`Could not find image ${filepath}`)
        }        
    }

    this.showImage = function(filepath, save_as_last_command=true){
        console.info(`screen: showing image ${filepath}`);
        command = `convert -resize ${screen_width}x${screen_height}! ${filepath} bgra:/dev/fb0`
        if(_enabled){
            exec.run(command);
            if(save_as_last_command){
                _last_exec_command = command;
            }
            console.debug(`screen: command ran was ${_last_exec_command}`)
        } else {
            console.warn(`screen: screen is disabled, would have ran: ${command}`)
        }
    }    

    this.showPreviousImage = function(){
        if(_last_exec_command != "" && _enabled){
            console.info(`debug: re-running last screen command ${_last_exec_command}`);
            exec.run(_last_exec_command);
        }
    }


}
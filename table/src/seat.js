module.exports = seat
var Led = require('./led');


function seat(id, button_1_keystroke, button_2_keystroke, button_3_keystroke, button_4_keystroke, led_1_pin, led_2_pin, enable_leds) {

  var self = this;
  
  var _id = id
  var _enable_leds = enable_leds;
  var _b1 = button_1_keystroke;
  var _b2 = button_2_keystroke;
  var _b3 = button_3_keystroke;
  var _b4 = button_4_keystroke;
  
  var _b1_led = new Led(led_1_pin, enable_leds);
  var _b2_led = new Led(led_2_pin, enable_leds);

  var _custom_name = ""

 
  // used for player name generation and saving led color
  var _colors = [
    { name: null, red: 0, green: 0, blue: 0 },
    { name: "red", red: 255, green: 0, blue: 0 },
    { name: "yellow", red: 255, green: 140, blue: 0 },
    { name: "green", red: 0, green: 255, blue: 0 },
    { name: "blue", red: 0, green: 0, blue: 255 },
    { name: "teal", red: 0, green: 255, blue: 255 },
    { name: "purple", red: 255, green: 0, blue: 255 },
    { name: "brown", red: 210, green: 105, blue: 30 },
    { name: "white", red: 255, green: 255, blue: 255 }
  ];
  var _color_index = 0;
  var _color = _colors[0];
  var _admin_color_1 = { name: "admin_color_1", red: 248, green: 248, blue: 217 }
  var _admin_color_2 = { name: "admin_color_2", red: 217, green: 248, blue: 217 }
  var _admin_color_3 = { name: "admin_color_3", red: 217, green: 50, blue: 50 }
  var _pass_color = { name: "pass_color", red: 248, green: 217, blue: 217 }


  var _status = null;


  // Actions
  this.incrementColor = function (){
    if (_color_index == _colors.length - 1) {
      _color_index = 0;
    } else {
      _color_index += 1;
    }
    _color = _colors[_color_index];
  }
  this.LEDAdminB1Available = function(){
    _b1_led.setColor(_admin_color_1)
    _b1_led.turnOn(1);
  }
  this.LEDAdminB2Available = function(){
    _b2_led.setColor(_admin_color_2)
    _b2_led.turnOn(1);
  }
  this.LEDAdminB1Danger = function(){
    _b1_led.setColor(_admin_color_3)
    _b1_led.turnOn(1);
  }
  this.LEDAdminB2Danger = function(){
    _b2_led.setColor(_admin_color_3)
    _b2_led.turnOn(1);
  }
  this.LEDOff = function(){
    _b1_led.turnOff();
    _b2_led.turnOff();
  }



  // Properties
  this.getID = function () { return _id }
  this.getB1 = function () { return _b1 }
  this.getB2 = function () { return _b2 }
  this.getB3 = function () { return _b3 }
  this.getB4 = function () { return _b4 }
  this.getB = function(button_number){
    if(button_number == 1){
      return _b1
    } else if (button_number == 2){
      return _b2    
    } else if (button_number == 3){
      return _b3
    } else if (button_number == 4){
      return _b4
    } else {
      return null
    }
  }
  this.getB1LED = function () { return _b1_led }
  this.getB2LED = function () { return _b2_led }
  this.getColor = function(){ return _color;}
  this.isFilled = function(){return _color.name != null}

  this.getStatus = function(){return _status}
  this.setStatus = function(status){_status = status; updateLEDs();}
  
  this.clearStatus = function(status){_status = null; updateLEDs();}
  this.clearColor = function(status){ _color_index = 0; _color = _colors[0]; self.LEDOff();}
 

  // display
  this.getStatusDisplay = function(){
    switch(_status){
      case "passed":
        return "Passed"
        break;

      case "in":
        return "Waiting"
        break;

      case "active":
      case "active-passable":
        return "Active"
        break;

      default:
        return _status
    }
  }

  this.setCustomName = function(name) {
    _custom_name = name
  }

  this.clearCustomName = function(){
    _custom_name = null
  }

  this.getName = function() {
    if(_color.name == null || _color.name == ""){
      return "Empty Seat"

    }else if (_custom_name != null && _custom_name != "") {
      return _custom_name

    }else if (_status == null) {
      return `${_color.name} Player`

    } else {
      return `${_color.name}`

    }
    

  }
  this.toString = function () {
    if(_color.name == null || _color.name == ""){
      return "Empty Seat"

    }else if (_custom_name != null && _custom_name != "") {
      return _custom_name

    }else if (_status == null) {
      return `${_color.name} Player`

    } else {
      return `${_color.name}`

    }
  }
 
  this.toData = function (){
    return {
      id: _id,
      b1: _b1,
      b2: _b2,
      b3: _b3,
      b4: _b4,
      current_color: _color,
      leds_enabled: _enable_leds,
      name: self.getName(),
      pass_color: _pass_color,
      status: _status,
      status_display: self.getStatusDisplay()
    }
  }

  // private
  function updateLEDs(){
    switch(_status){
          case "in":
            _b1_led.setColor(_color);
            _b1_led.turnOn(1);

            _b2_led.turnOff()
          break;

          case "passed":
            _b1_led.setColor(_color)
            _b1_led.turnOn(0.05);
    
            _b2_led.turnOff();
          break;

          case "dead":
            _b1_led.turnOff();    
            _b2_led.turnOff();
          break;

          case "active":
            _b1_led.setColor(_color)
            _b1_led.alert(0,1);
            
            _b2_led.turnOff()
          break;  

          case "active-passable":
            _b1_led.setColor(_color)
            _b1_led.alert(0,1);
            _b2_led.setColor(_pass_color)
            _b2_led.turnOn(1)
          break;  

          // error state
          default: 
            _b1_led.setColor(_admin_color_1)
            _b1_led.alert(0,1);
            _b2_led.setColor(_admin_color_1)
            _b2_led.alert(0,1);
        }
  }
}
module.exports = logger
var FileSystem = require('fs')


function logger(log_file){

    var _log_file = log_file;

    this.consoleMsg = function(level, msg){

        var l = level.toUpperCase();
        var m;
        var c;

        switch(level){
            case "INFO":
                c = "\u001b[37;1m";
            break;
            case "WARN":
                c = "\u001b[33;1m";
            break;
            case "ERROR":
                c = "\u001b[31;1m";
            break;
            case "DEBUG":
                c = "\u001b[34;1m";
            break;
            default:
                c = "\u001b[37m";
        }

       m = `${new Date().toLocaleString()} [${c}${l}\u001b[0m]: ${msg.replace( /\\n/g, '    ' )}`
       console.log(m);
    }
    this.logMsg = function(level, msg){
        var l = level;
        var m = `${new Date().toLocaleString()} [${l.toUpperCase()}]: ${msg.replace( /\\n/g, '    ' )}\n`
        FileSystem.appendFile(_log_file, m, function (err) {
            if (err) throw err;
        });
    }

}
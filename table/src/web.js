module.exports = web

const express = require('express')
const socketIo = require('socket.io')
const http = require('http')



function web(enabled, server_address, server_port, client_address, client_port, table) {

    var self = this;

    var _enabled = enabled;
    var _table = table;
    var _port = server_port;
    const app = express()
    const server = http.createServer(app)
    const io = socketIo(server, { 
        cors: {
            origins: [`http://${server_address}:${server_port}`,`http://${client_address}:${client_port}`]
        }
    })



    this.sendUpdateToFrontend = function(){
        console.debug(`web: sending table update to frontend`)
        io.to('bgt').emit('table', _table.toData())            
    }
    this.sendSoundToFrontend = function(sound){
        console.debug(`web: sending sound effect to frontend`)
        io.to('bgt').emit('sound', sound)            
    }
    
    this.start = function(){
        if(_enabled){
                        

            io.on('connection',(socket)=>{
                socket.join('bgt')
                console.info(`web: client ${socket.id} connected`)
                self.sendUpdateToFrontend();     

                socket.on('pressTableKey', function(data){
                    if(data != null && data.keypress != null && data.keypress != ""){
                        console.info(`web: client frontend sent keypress ${data.keypress}`)
                        _table.processButtonPress(data.keypress)                                                

                    }
                });

                socket.on('playerNameUpdate', function(data){      
                    var seats = _table.getSeats();

                    if(data != null && data.seatIndex != null && data.newName != null && data.newName != ""){                        
                        console.info(`web: player name update received, setting seat ${data.seatIndex} to ${data.newName}`)                                                
                        seats[data.seatIndex].setCustomName(data.newName)

                    } else if(data.seatIndex != null){
                        console.info(`web: player name update received, clearing name on seat ${data.seatIndex}`)                                                
                        seats[data.seatIndex].clearCustomName()
                    }    
                });
                                
                socket.on('disconnect',(reason)=>{
                    console.info(`web: client ${socket.id} disconnected -> ${reason}`)
                })

            })


          
                          
            _table.setButtonPressCallback(function(){
                io.to('bgt').emit('table', _table.toData())            
            });
                             
            server.listen(_port, '0.0.0.0', err=> {
                if(err) console.log(err)
                console.info('web: server started on port ' + _port)
            })           
        } else {
            console.warn(`web: web interface disabled, ignoring startup request for webserver on port ${_port}`)

        }
    }

    app.get('/', (req, res) => {
        res.send(`Jay's Gaming Table is online`)
    }) 

    app.get('/set-buttons', (req,res) => {

        var seat_numbers = ['1','2','3','4','5','6']
        if(req.query.seats != null){
            var seat_numbers = req.query.seats.split(",")
        }

        var button_numbers = ['1', '2']
        if(req.query.buttons != null){
            var button_numbers = req.query.buttons.split(",")
        }

        var red = 255
        if(req.query.red != null){
            red = parseInt(req.query.red)
        }
        var blue = 255
        if(req.query.blue != null){
            blue = parseInt(req.query.blue)
        }
        var green = 255
        if(req.query.green != null){
            green = parseInt(req.query.green)
        }
        var brightness = 255
        if(req.query.brightness != null){
            brightness = parseInt(req.query.brightness)
        }

        var seats = table.getSeats();

        for(var i = 0; i < seats.length; i++){

            var seat = seats[i];        
            if(seat_numbers.includes(seat.getID().toString())){        
            
                if(button_numbers.includes('1')){
                    var led1 = seat.getB1LED()
                    led1.setColor({red: red, green: green, blue: blue})
                    led1.applyColor(brightness)    
                }
                if(button_numbers.includes('2')){
                    var led2 = seat.getB2LED()
                    led2.setColor({red: red, green: green, blue: blue})
                    led2.applyColor(brightness)    
                }                            
            }
        }
        
        res.send(`Buttons [${button_numbers}] on seats [${seat_numbers}] have been set to R:${red} G:${green} B:${blue} at brightness ${brightness}`)
    })
    app.post('/set-buttons', (req,res) => {

        var seat_numbers = ['1','2','3','4','5','6']
        if(req.query.seats != null){
            var seat_numbers = req.query.seats.split(",")
        }

        var button_numbers = ['1', '2']
        if(req.query.buttons != null){
            var button_numbers = req.query.buttons.split(",")
        }

        var red = 255
        if(req.query.red != null){
            red = parseInt(req.query.red)
        }
        var blue = 255
        if(req.query.blue != null){
            blue = parseInt(req.query.blue)
        }
        var green = 255
        if(req.query.green != null){
            green = parseInt(req.query.green)
        }
        var brightness = 255
        if(req.query.brightness != null){
            brightness = parseInt(req.query.brightness)
        }

        var seats = table.getSeats();

        for(var i = 0; i < seats.length; i++){

            var seat = seats[i];        
            if(seat_numbers.includes(seat.getID().toString())){        
            
                if(button_numbers.includes('1')){
                    var led1 = seat.getB1LED()
                    led1.setColor({red: red, green: green, blue: blue})
                    led1.applyColor(brightness)    
                }
                if(button_numbers.includes('2')){
                    var led2 = seat.getB2LED()
                    led2.setColor({red: red, green: green, blue: blue})
                    led2.applyColor(brightness)    
                }                            
            }
        }
        
        res.send(`Buttons [${button_numbers}] on seats [${seat_numbers}] have been set to R:${red} G:${green} B:${blue} at brightness ${brightness}`)
    })

}
















 // // EXPRESS API
            // const app = express()
            // const server = http.createServer(app);

            // const io = new Server(server,{ 
            //     cors: {
            //       origin: 'http://localhost:3000'
            //     }
            // }) //in case server and client run on different urls
            // io.on('connection',(socket)=>{
            //   console.log('client connected: ',socket.id)
              
            //   socket.join('clock-room')
              
            //   socket.on('disconnect',(reason)=>{
            //     console.log(reason)
            //   })
            // })
            // setInterval(()=>{
            //      io.to('clock-room').emit('time', new Date())
            // },1000)




            // io.on('connection', (socket) => {
            //     console.log('a user connected');
            // });

            // // SEATS
            // // app.get('/api/v1/seats', (req, res) => {
            // //     var message = ""
            // //     var success = true;
            // //     var data = []

            // //     console.debug(`Web API: requesting all seats `)            
            // //     for(var i = 0; i < seats.length; i++){
            // //         data.push(_table.getSeats()[i].toData());
            // //     }               
            // //     res.send({action: req.originalUrl, success: success, message: message, timestamp: Date.now(), data: data}) 
            // // })
            // // app.get('/api/v1/seat/:seatNumber', (req, res) => {
            // //     var seat_number = req.params.seatNumber
            // //     var message = ""
            // //     var data = {};
            // //     var success = true;

            // //     console.debug(`Web API: fetching seat number ${seat_number}`)        
            // //     if(seat_number == "admin"){
            // //         seat_number = 1;
            // //     }
            // //     var seat = _table.getSeats()[seat_number-1]
            // //     if(seat == null){
            // //         throw "Seat does not exist"
            // //     }
            // //     data = seat.toData();                 
            // //     res.send({action: req.originalUrl, success: success, message: message, timestamp: Date.now(), data: data}) 
            // // })
            // // app.get('/api/v1/seat/:seatNumber/press/:buttonNumber', (req, res) => {
            // //     var seat_number = req.params.seatNumber
            // //     var button_number = req.params.buttonNumber
            // //     var message = ""
            // //     var success = true;

            // //     console.debug(`Web API: pressing seat number ${seat_number} button number ${button_number}`)                
            // //     if(seat_number == "admin"){
            // //         seat_number = 1;
            // //     }
            // //     var seat = _table.getSeats()[seat_number-1]
            // //     if(seat == null){
            // //         throw "Seat does not exist"
            // //     }
            // //     var button_key = seat.getB(button_number)
            // //     if(button_key == null){
            // //         throw "Button does not exist"
            // //     }
            // //     table.processButtonPress(button_key)                    
            // //     res.send({action: req.originalUrl, success: success, message: message, timestamp: Date.now()}) 
            // // })

            // // // GAMES
            // // app.get('/api/v1/games', (req, res) => {
            // //     var message = ""
            // //     var success = true;
            // //     var data = []

            // //     console.debug(`Web API: requesting all games `)                
            // //     for(var i = 0; i < _table.getGames().length; i++){
            // //         data.push(_table.getGames()[i].toData());
            // //     }              
            // //     res.send({action: req.originalUrl, success: success, message: message, timestamp: Date.now(), data: data}) 
            // // })
            // // app.get('/api/v1/game/current', (req, res) => {
            // //     var message = ""
            // //     var success = true;
            // //     var data = {}

            // //     console.debug(`Web API: requesting all games `)        
            // //     data = _table.getGame()
            // //     res.send({action: req.originalUrl, success: success, message: message, timestamp: Date.now(), data: data}) 
            // // })

            // // // TABLES
            // // app.get('/api/v1/table', (req, res) => {
            // //     var message = ""
            // //     var success = true;
            // //     var data = []

            // //     console.debug(`Web API: requesting all games `)                
            // //     data = _table.toData();           
            // //     res.send({action: req.originalUrl, success: success, message: message, timestamp: Date.now(), data: data}) 
            // // })

            // // // OTHER
            // app.get('/', (req, res) => {
            //     res.send(`Jay's Gaming Table is online`)
            // }) 
            // // app.get('/api/v1/reset', (req, res) => {
            // //     var message = ""
            // //     var success = true;
            
            // //     console.debug("Web API: table reset called")                
            // //     _table.reset();                
            // //     res.send({action: req.originalUrl, success: success, message: message, timestamp: Date.now()}) 
            // // }) 
            // // app.get('/api/v1/exit', (req, res) => {
            // //     var message = ""
            // //     var success = true;
            
            // //     console.debug("Web API: exit called")                
            // //     _table.reset(); 
            // //     setTimeout(function(){
            // //         process.exit(0);
            // //     },2000)                            
            // //     res.send({action: req.originalUrl, success: success, message: message, timestamp: Date.now()}) 
            // // })


            // app.listen(_port, () => {
            //     console.info(`Web Server started on port ${_port}`);
            // })
module.exports = keyboard
var keypress = require('keypress');
var tty = require('tty');

function keyboard(enabled, table) {

    var _enabled = enabled;
    var _table = table;

    var jam_queue = 0;
    var input_jammed = false;

    const reset_key = table.getP1().getB4();

    // This interval is constantly attempting to count down to zero
    // whenever a key is pressed we increment the queue
    // if the queue is ever too high we know we're jammed and 
    // all we need to do is unjam and wait for this to get back to zero
    // to trigger an unjam
    const jam_inteval= setInterval(() => {
        if(jam_queue <= 0){
            jam_queue = 0
            if(input_jammed){
                input_jammed = false;
                _table.getSpeaker().playSystemSound("return", 70)
                _table.getScreen().showPreviousImage();
            }
        } else {
            jam_queue -= 1;
        }
    }, 50);

    this.start = function(){
        // Setup stdin listener
        keypress(process.stdin);
        console.debug("Key listener ready")

        console.info(`Keyboard interface started, press ctrl+c to exit\n`)
        process.stdin.on('keypress', function (ch, key) {

            console.debug("Jam queue: " + jam_queue)

            // Parse Keystroke
            key_clean = null;
            if(key && typeof(key.name) != "undefined"){
                key_clean = key.name;
            } else if (ch && typeof(ch) != "undefined"){
                key_clean = ch.toString();
            } 

            // Error state
            if (key_clean == null) {
                _table.getSpeaker().playSystemSound("error", 90)
                console.error("unknown key")
                console.error(key)
                console.error(ch)

            } else {

                //Exit
                if (key_clean == "c" && key.ctrl) {
                    _table.reset();
                    process.stdin.pause();
                    setTimeout(function(){
                        process.exit(0);
                    },2000)

                // Detect if keypresses are coming in too fast
                // this would indicate a stuck button
                } else if(jam_queue > 10 && !input_jammed){
                    input_jammed = true;
                    setTimeout(function(){
                        _table.getSpeaker().playSystemSound("error", 90)
                        _table.getScreen().showJammed();
                    },2000)  
                
                // if we ARE jammed, set the jam queue to a rasonable level on
                // each new keystroke so it won't take forever to clear
                } else if(input_jammed){
                    jam_queue = 5
                    // and do NOT process the keystroke
                
            
                // Regular Keypress, process it
                }else{

                    // add to the jam queue
                    jam_queue += 1;

                    // Reset table
                    if(key_clean == reset_key){
                        _table.reset();  
                    } else {
                        _table.processButtonPress(key_clean)
                    }

                    // Debug info
                    console.debug(`table status:\n${_table.toString()}Table Mode:\t\t${_table.getMode()}\nGame:\t\t\t${ _table.getGame().getName()}\nGame Mode:\t\t${ _table.getGame().getCurrentGameMode().name}\nLast Keypress:\t\t${key_clean}`);
                }
            
            }
        });
        if (typeof process.stdin.setRawMode == 'function') {
            process.stdin.setRawMode(true);
        } else {
            tty.setRawMode(true);
        }
        process.stdin.resume();
    }

}